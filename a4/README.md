# LIS4381 - Mobile Web Application Development

## Kelly Curran

### Assignment 4 Requirements:

*Four Parts:*

1. Create Online Portfolio using Apache and Bootstrap
2. Use iQuery to validate client side data
3. Link to local host web app
4. Screenshots of SkillSets 10-12



#### Assignment Screenshots:

*Link to Local LIS4381 Web App:*

https://localhost/lis4381/index.php

*Screenshot of Main Page running*:

![Main Page Screenshot](img/mainpage.png)

*Screenshot of Failed Validation*:

![Failed Validation Screenshot](img/failed.png)

*Screenshot of Passed Validation*:

![Passed Validation Screenshot](img/passed.png)

*Screenshot of Skillsets 10-12*:

*Screenshot of Array List*:
![SS10: Array List](img/ss10.png)

*Screenshot of Alpha Numeric Special*:
![SS11: Alpha Numeric Special](img/ss11.png)

*Screenshot of Temperature Conversion*:
![SS12: Temperature Convertsion](img/ss12.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/kellerzzzcurran/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")
