

# LIS 4381 - Mobile Web Applications Development

## Kelly Curran

### Assignment 2 Requirements:

*Three Parts:*

1. Create Healthy Recipes Android Application
2. Provide screenshots of compiled/running app
3. Skillsets 1-3
4. BitBucket Repository Links:

    a) https://bitbucket.org/kellerzzzcurran/lis4381/src/master/

    b) https://bitbucket.org/kellerzzzcurran/bitbucketstationlocations/src/master/


#### README.md file should include the following items:

* Screenshot of running application's first user interface
* Screenshot of running application's second user interface

> 
> 

#### Assignment Screenshots:

*Screenshot of first user interface running*:

![First User Interface Running](img/recipe.png)

*Screenshot of second user interface running*:

![Second User Interface Running](img/instructions.png)

*Screenshot of Skillsets 1-3
*Screenshot of Even or Odd*:
![SS1: Even or Odd](img/Even_or_Odd.png)

*Screenshot of Largest of Two Integers*:
![SS2: Largest of Two Integers](img/Largest_of_Two.png)

*Screenshot of Arrays and Loops*:
![SS3: Arrays and Loops](img/Arrays_and_Loops.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A2 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/kellerzzzcurran/bitbucketstationlocations/src/master// "Bitbucket Station Locations")


