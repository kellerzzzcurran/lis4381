

# LIS 4381 - Mobile Web Applications Development

## Kelly Curran

### Project 1 Requirements:

*4 Parts:*

1. Create "My Business Card" Application on Android Studio
2. Provide Screenshots of Compiled/Running App
3. Complete SkillSets 7-9
4. Provide Screenshots of SkillSet Codes Running

#### README.md file should include the following items:

* Screenshot of running application's first user interface
* Screenshot of running application's second user interface
* Screenshot of skillsets 7-9 code running

> 
>

#### Assignment Screenshots:

*Screenshot of first user interface running*:

![First User Interface Running](img/card1.png)

*Screenshot of second user interface running*:

![Second User Interface Running](img/card2.png)

*Screenshot of Skillsets 7-9

*Screenshot of Random Array Data*:
![SS7: Random Array Data](img/ss7.png)

*Screenshot of Larger Three Numbers*:
![SS8: Larger Three Numbers](img/ss8.png)

*Screenshot of Array Runtime Data Validation*:
![SS9: Array Runtime Data Validation](img/ss9.png)



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[P1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/kellerzzzcurran/bitbucketstationlocations/src/master// "Bitbucket Station Locations")
