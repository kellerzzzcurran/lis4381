

# LIS4381 - Mobile Web Application Development

## Kelly Curran

### Assignment 5 Requirements:

*4 Parts:*

1. Link Online Portfolio to MySQL Data
2. Use Online Portfolio to Validate Server Side Data
3. Complete SkillSets 13-15
4. Provide Screenshots of SkillSets 13-15

#### Link to Local LIS4381 Web App:

* http://localhost:8080/lis4381/index.php



#### Assignment Screenshots:

*Screenshot of A5 Main Page*:

![Main Page Screenshot](img/main.png)

*Screenshot of Invalid Added Data*:

![Invalid Screenshot](img/addinvalid.png)

*Screenshot of Failed Validation*:

![Failed Validation Screenshot](img/failedvalidation.png)

*Screenshot of Valid Added Data*:

![Valid Screenshot](img/addvalid.png)

*Screenshot of Passed Validation*:

![Passed Validation Screenshot](img/passedvalidation.png)


*Screenshot of Skillsets 13-15*:

*Screenshot of Sphere Volume Calculator*:
![SS13: Sphere Volume Calculator](img/ss13.png)

*Screenshots of PHP Simple Calculator*:
![SS14: PHP Simple Calculator](img/simplecalcadd.png)
![SS14: PHP Simple Calculator](img/simplecalcadda.png)
![SS14: PHP Simple Calculator](img/simplecalcdiv.png)
![SS14: PHP Simple Calculator](img/simplecalcdiva.png)

*Screenshots of PHP Write/Read File*:
![SS15: PHP Write/Read File](img/index.png)
![SS15: PHP Write/Read File](img/process.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/kellerzzzcurran/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")


