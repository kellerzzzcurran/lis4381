import java.util.Scanner;

public class Methods

{
    public static String name;
    public static int age;

    public static void getRequirements()
    {
        System.out.println("\nDeveloper: Kelly Curran");
        System.out.println("Program prompts user for first name and age, then prints results.\n");
    }

    public static void getUserInput()
    {
        Scanner src = new Scanner(System.in);

        System.out.print("Enter first name: ");
        name = src.nextLine();
        
        System.out.print("Enter age: ");
        age = src.nextInt();

        myVoidMethod(name, age);
        System.out.println("value-returning method call: " + myValueReturningMethod(name, age) + "\n");
    }

    public static void myVoidMethod(String name, int age)
    {
        System.out.println("\nvoid method call: " + name + " is " + age);
    }

    public static String myValueReturningMethod(String name, int age)
    {
        String output = name + " is " + age;
        return output;
    }
}