import java.util.Scanner;
import java.util.Random;

public class Methods

{

    public static void getRequirements()
    {
        System.out.println("\nDeveloper: Kelly Curran\n");
        System.out.println("Program determines whether user-entered value is alpha, numeric, or special character.");

        System.out.println();
    }

    public static void determineChar()
    {
        Scanner sc = new Scanner(System.in);     
            
        System.out.print("Enter character: ");
        char ch = sc.next().charAt(0);

        if((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z'))
        {
            System.out.println(ch + " is alpha. ACII value: " + (int) ch);
        }
        else if (ch >= '0' && ch <= '9')
        {
            System.out.println(ch + " is numeric. ACII value: " + (int) ch);
        }
        else
        {
            System.out.println(ch + " is special character. ACII value: " + (int) ch);
        }
            
    }

}