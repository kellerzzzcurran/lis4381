
public class Methods

{

    static String[] arr = new String[] { "dog", "cat", "bird", "fish", "insect" };

    public static void forLoop()
    {   
        System.out.println("for loop:");       
        for(int i=0; i < arr.length; i++)
        {
            System.out.println(arr[i]);
        }   
        System.out.println();
    }

    public static void enhancedForLoop()
    {    
        System.out.println("enhanced for loop:");        
        for (String item : arr) 
        {
            System.out.println(item);
        }    
        System.out.println();   
    }

    public static void whileLoop()
    {
        System.out.println("while loop:");
        int i = 0;
        while(i < arr.length)
        {
            System.out.println(arr[i]);
            i++;
        }
        System.out.println();    
    }

    public static void doWhileLoop()
    {
        System.out.println("do-while loop:");
        int i = 0;
        do
        {
            System.out.println(arr[i]);
            i++;
        } while (i < arr.length); 
        System.out.println();
    }
}