
public class Main 
{
    public static void main(String args[])
    {   
        System.out.println("\nDeveloper: Kelly Curran\n");
        System.out.println("Program loops through array of strings\n");

        Methods.forLoop();
        Methods.enhancedForLoop();
        Methods.whileLoop();
        Methods.doWhileLoop();
        
    }
}
