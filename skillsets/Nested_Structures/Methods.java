import java.util.Scanner;

public class Methods

{
    public static void nestedStructures()
    {
        int arr[] = { 3, 2, 4, 99, -1, -5, 3, 7 };
        
        Scanner src = new Scanner(System.in);
            
            System.out.println("Array length: 8");
            System.out.print("Enter search value: ");
            int num1 = src.nextInt();
            
            System.out.println();
            for (int i=0; i < arr.length; i++) {
                if(num1 == arr[i]){
                    System.out.println(num1 + " found at index " + arr[i]);
                }
                else {
                    System.out.println(num1 + " *not* found at index " + arr[i]);
                }
            }
    }
}