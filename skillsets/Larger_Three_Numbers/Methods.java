import java.util.Scanner;
import java.util.Random;

public class Methods

{

    public static void getRequirements()
    {
        System.out.println("\nDeveloper: Kelly Curran");
        System.out.println("Program evaluates largest of three integers.\n");
        System.out.println("Note: Program checks for integers and non-numeric values.");

        System.out.println();
    }

    public static void validateUserInput()
    {
        Scanner src = new Scanner(System.in);
        int num1 = 0; int num2 = 0; int num3 = 0;
            
            System.out.print("Please enter first number: ");
            while (!src.hasNextInt())
            {
                System.out.println("Not valid integer!\n");
                src.next();
                System.out.print("Please try again. Enter first number: ");
            }
            num1 = src.nextInt();

            System.out.print("Please enter second number: ");
            while (!src.hasNextInt())
            {
                System.out.println("Not valid integer!\n");
                src.next();
                System.out.print("Please try again. Enter second number: ");
            }
            num2 = src.nextInt();

            System.out.print("Please enter third number: ");
            while (!src.hasNextInt())
            {
                System.out.println("Not valid integer!\n");
                src.next();
                System.out.print("Please try again. Enter third number: ");
            }
            num3 = src.nextInt();

            System.out.println();

        getLargestNumber(num1, num2, num3);
    
    }

    public static void getLargestNumber(int num1, int num2, int num3)
    {
        System.out.println("Numbers entered: " + num1 + ", " + num2 + ", " + num3);

        if (num1 > num2 && num1 > num3)
            System.out.println("First number is largest.");
        else if (num2 > num1 && num2 > num3)
            System.out.println("Second number is largest.");
        else if (num3 > num1 && num3 > num2)
            System.out.println("Third number is largest.");
        else
            System.out.println("Integers are equal");
    }

}