import java.util.Scanner;

public class Methods

{
    public static void decisionStructures()
    {
        Scanner src = new Scanner(System.in);
            
            System.out.print("Enter phone type: ");
            char letter = src.next().charAt(0);
            
            System.out.println("\nif...else: ");
            if(letter == 'W' || letter == 'w')
            {
                System.out.println("Phone type: work\n");
            }
            else if(letter == 'C' || letter == 'c')
            {
                System.out.println("Phone type: cell\n");
            }
            else if(letter == 'H' || letter == 'h')
            {
                System.out.println("Phone type: home\n");
            }
            else if(letter == 'N' || letter == 'n')
            {
                System.out.println("Phone type: none\n");
            }
            else {
                System.out.println("Incorrect character entry\n");
            }

            System.out.println("switch: ");
            switch (letter) {
                case 'W':
                case 'w': {
                    System.out.println("Phone type: work\n");
                    break;
                }
                case 'C':
                case 'c': {
                    System.out.println("Phone type: cell\n");
                    break;
                }
                case 'H':
                case 'h': {
                    System.out.println("Phone type: home\n");
                    break;
                }
                case 'N':
                case 'n': {
                    System.out.println("Phone type: none\n");
                    break;
                }
                default: {
                    System.out.println("Incorrect character entry\n");
                }
            }
    }
}