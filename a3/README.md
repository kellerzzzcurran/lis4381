

# LIS 4381 - Mobile Web Applications Development

## Kelly Curran

### Assignment 3 Requirements:

*7 Parts:*

1. Create "My Event" Application on Android Studio
2. Provide Screenshots of Compiled/Running App
3. Create Database on MySQL
4. Provide Screenshot of ERD
5. Provide Screenshots of 10 Records for Each Table
6. Complete SkillSets 4-6
7. Provide Screenshots of SkillSet Codes Running

#### README.md file should include the following items:

* Screenshot of ERD;
* Screenshot of running application's first user interface;
* Screenshot of running application's second user interface;
* Screenshots of 10 records for each table;
* Links to the following files:

    a) https://bitbucket.org/kellerzzzcurran/lis4381/src/master/a3/docs/a3.mwb

    b) https://bitbucket.org/kellerzzzcurran/lis4381/src/master/a3/docs/a3.sql


> 
> 

#### Assignment Screenshots:

*Screenshot of ERD*:

![ERD Screenshot](img/erd.png)

*Screenshot of running first user interface*:

![First User Interface](img/firstinterface.png)

*Screenshot of running second user interface*:

![Second User Interface](img/secondinterface.png)

*Screenshot of 10 records for each table*:

![Records for Petstore Table](img/petstore.png)
![Records for Customer Table](img/customer.png)
![Records for Pet Table](img/pet.png)

*Screenshot of Skillsets 4-6*:

*Screenshot of Decision Structures*:
![SS4: Decision Structures](img/decision.png)

*Screenshot of Nested Structures*:
![SS5: Nested Structures](img/nested.png)

*Screenshot of Methods*:
![SS6: Methods](img/method.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/kellerzzzcurran/bitbucketstationlocations/src/master// "Bitbucket Station Locations")


