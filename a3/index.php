<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Kelly Curran">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment 3</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> Assignment 3 requires the creation of the application "My Event" on Android Studio. The application simulates purchasing concert tickets for various bands at various prices. Assignment 3 also requires the creation of an ERD on MySQL containing 10 records for each table. 
				</p>

				<h4>MySQL ERD</h4>
				<img src="img/erd.png" class="img-responsive center-block" alt="MySQL ERD">

				<h4>MySQL First Records</h4>
				<img src="img/pet.png" class="img-responsive center-block" alt="MySQL First Records">

				<h4>MySQL Second Records</h4>
				<img src="img/petstore.png" class="img-responsive center-block" alt="MySQL Second Records">

				<h4>MySQL Third Records</h4>
				<img src="img/customer.png" class="img-responsive center-block" alt="MySQL Third Records">

				<h4>App First User Interface</h4>
				<img src="img/firstinterface.png" class="img-responsive center-block" alt="App First User Interface">

				<h4>App Second User Interface</h4>
				<img src="img/secondinterface.png" class="img-responsive center-block" alt="App Second User Interface">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
