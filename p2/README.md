# LIS4381 - Mobile Web Application Development

## Kelly Curran

### Project 2 Requirements:

*3 Parts:*

1. Update Online Portfolio using Assignment 5 Data
2. Use Online Portfolio to Include Edit and Delete Functionality
3. Successfully Provide Screenshots of Editing and Deleting Records

#### Link to Local LIS4381 Web App:

* http://localhost:8080/lis4381/index.php



#### Assignment Screenshots:

*Screenshot of P2 Carousel*:

![Carousel Screenshot](img/carousel.png)

*Screenshot of Main Page*:

![Main Page Screenshot](img/p2main.png)

*Screenshot of Edit Page*:

![Edit PageScreenshot](img/p2edit.png)

*Screenshot of Failed Validation*:

![Failed Validation Screenshot](img/p2fail.png)

*Screenshot of Passed Validation*:

![Passed Validation Screenshot](img/p2passed.png)

*Screenshot of Delete Prompt*:
![Delete Prompt Screenshot](img/p2prompt.png)

*Screenshot of Deleted Record*:
![Deleted Record Screenshot](img/p2delete.png)



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/kellerzzzcurran/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")


