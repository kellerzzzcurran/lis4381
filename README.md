> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Kelly Curran

### LIS 4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    * Install AMPPS
      * Screenshot of AMPPS running
    * Install Java Developer Kit
      * Screenshot of JDK java "Hello"
    * Install Visual Studio Code
    * Install Android Studio
      * Screenshot of Android Studio "My First App"
    * Provide git command descriptions
    * Create Bitbucket Repository
    * Complete Bitbucket tutorial (bitbucketstationlocations)

2. [A2 README.md](a2/README.md "My A2 README.md file")
    * Create "Healthy Recipes" Application on Android Studio
    * Provide Screenshots of 2 User Interfaces Running
    * Complete SkillSets 1-3
    * Provide Screenshots of SkillSet Code Running

3. [A3 README.md](a3/README.md "My A3 README.md file")
    * Create "My Event" Application on Android Studio
    * Provide Screenshots of 2 User Interfaces Running
    * Create Database on MySQL
    * Provide Screenshot of ERD
    * Provide Screenshots of 10 Records for Each Table
    * Complete SkillSets 4-6
    * Provide Screenshots of SkillSet Code Running

4. [A4 README.md](a4/README.md "My A4 README.md file")
    * Create Online Portfolio using Apache and Bootstrap
    * Use iQuery to Validate Client Side Data
    * Link to Local Host Web App
    * Complete SkillSets 10-12
    * Provide Screenshots of SkillSets 10-12

5. [A5 README.md](a5/README.md "My A5 README.md file")
    * Link Online Portfolio to MySQL Data
    * Use Online Portfolio to Validate Server Side Data
    * Complete SkillSets 13-15
    * Provide Screenshots of SkillSets 13-15

6. [P1 README.md](p1/README.md "My P1 README.md file")
    * Create "My Business Card" Application on Android Studio
    * Provide Screenshots of Compiled/Running App
    * Complete SkillSets 7-9
    * Provide Screenshots of SkillSet Codes Running

7. [P2 README.md](p2/README.md "My P2 README.md file")
    * Update Online Portfolio using Assignment 5 Data
    * Use Online Portfolio to Include Edit and Delete Functionality
    * Successfully Provide Screenshots of Editing and Deleting Records

