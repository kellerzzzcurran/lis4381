

# LIS 4381 - Mobile Web Applications Development

## Kelly Curran

### Assignment 1 Requirements:

*Four Parts*

1. Distributed Version Control with Git and BitBucket
2. Development Installation
3. Questions
4. BitBucket Repository Links:

    a) https://bitbucket.org/kellerzzzcurran/lis4381/src/master/

    b) https://bitbucket.org/kellerzzzcurran/bitbucketstationlocations/src/master/

#### README.md file should include the following items:

* Screenshot of AMPPS running

* Screenshot of running JDK "java Hello"

* Screenshot of running Android Studio "My First App"

>
> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one
2. git status - Show the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push -  Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git clean - Remove untracked files from the working tree

#### Assignment Screenshots:

*Screenshot of AMPPS running*: 

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/java.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)




#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")


